# README #

---Chainize---

Chainize is a message forwarding application that users can ask for help or make announcment to their contacts in their phone number.

Resources:

* www.digits.com
* https://cocoapods.org
* http://www.awesometuts.com/
* http://firebase.google.com
* https://www.raywenderlich.com/

What is done ?

* Launch screen.
* User registration using 3rd framework. SMS authentication. 
* Adding user number and session info. to the user defaults. 
* Auto login if user was registered before.
* Asking user to access their contact list.
* Fetching  contacts number from user
* Installing Firebase
* Storing user to Database
* Creating Tableview and Chat Screen 
* Installing JSQMessagesViewController

Issues: 

* Multi user can send or recieve message.
* Back button doesn't appear in the chat screen. 
* Video and photos should be sent and recieved
* issues occured when retrieving msg from database (couldn't add to the project)