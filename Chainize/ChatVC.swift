//
//  ChatVC.swift
//  Chainize
//
//  Created by Doğukan Ağırbaşlı on 7/1/17.
//  Copyright © 2017 Doğukan Ağırbaşlı. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import MobileCoreServices
import AVKit



class ChatVC: JSQMessagesViewController {

    private var messages = [JSQMessage]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.senderId = "1"
        self.senderDisplayName = "Dogukan"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named:"ProfilePic"), diameter: 30)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!{
            
            let bubble = JSQMessagesBubbleImageFactory()
            //let message = messages [indexPath.item]
            
            return (bubble?.outgoingMessagesBubbleImage(with: UIColor.blue))!
            
      
    }
  
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
     override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section : Int ) -> Int {
        
        return messages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath : IndexPath ) -> UICollectionViewCell {
        
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as!  JSQMessagesCollectionViewCell
        
        return cell
     
    }

    

    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        messages.append(JSQMessage(senderId: senderId, displayName: senderDisplayName, text: text))
        collectionView.reloadData()
        
        //finishSendingMessage()
        
    }
    
    
    @IBAction func backBtn(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
