//
//  Constants.swift
//  Chainize
//
//  Created by Doğukan Ağırbaşlı on 7/1/17.
//  Copyright © 2017 Doğukan Ağırbaşlı. All rights reserved.
//

import Foundation

class Constants{
    
    //DB
    static let CONTACTS = "Contacts"
    static let MESSAGES = "Messages"
    static let MEDIA_MESSAGES = "Media_Messages"
    static let IMAGE_STORAGE = "Image_Storage"
    static let VIDEO_STORAGE = "Video_Storage"
    
    static let EMAIL = "email"
    static let PASSWORD = "password"
    static let DATA = "data"
    
    //Messages
    static let TEXT = "text"
    static let SENDER_ID = "sender_id"
    static let SENDER_NAME = "sender_name"
    static let URL = "url"
    
    
    
    
    
    
}
