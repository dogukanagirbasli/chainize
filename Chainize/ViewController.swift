//
//  ViewController.swift
//  Chainize
//
//  Created by Doğukan Ağırbaşlı on 5/5/17.
//  Copyright © 2017 Doğukan Ağırbaşlı. All rights reserved.
//

import UIKit
import DigitsKit
import Contacts
import ContactsUI
import Firebase


class ViewController: UIViewController, CNContactPickerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let entityType = CNEntityType.contacts
        let authStatus = CNContactStore.authorizationStatus(for: entityType)
        
        
        if authStatus == CNAuthorizationStatus.notDetermined{
            
            let contactStore = CNContactStore.init()
            
            contactStore.requestAccess(for: entityType, completionHandler: { (sucess, nil) in
                print("permission is given")
                
                
            })
            
            
        }else{
            print("permission has already given")
            
           
            let x = FetchUserContacts()
            let size =  x.contacts.count
            //print(size)
            for index in 0..<size{
                if let a = x.contacts[index].phoneNumbers.first?.value.stringValue{
                print(a)
               
                }
            }
            
        }
    

    }
    
    func navigateToMainAppScreen() {
        self.performSegue(withIdentifier: "homeScreen", sender: self)
    }
    
    @IBAction func GetStarted(_ sender: UIButton) {
        
        let configuration = DGTAuthenticationConfiguration(accountFields: .defaultOptionMask)

        
        // Start the Digits authentication flow with the custom appearance.
        Digits.sharedInstance().authenticate(with: nil, configuration:configuration!){ (session, error) in
            if session != nil {
                // We now have access to the user’s verified phone number and to a unique, stable, userID.
                
                
                if let userPhoneNumber = (session?.phoneNumber!)  {
                    UserDefaults.standard.set(userPhoneNumber, forKey: "phoneNumber")
                    print("\(userPhoneNumber)")
                }
                
                if let sessionID = (session?.userID!) {
                UserDefaults.standard.set(sessionID, forKey: "sessionID")
                print(sessionID)
                    
                }

                
                
                // Navigate to the main app screen.
                let deadline = DispatchTime.now() + .milliseconds(500) //.seconds(1)
                DispatchQueue.main.asyncAfter(deadline: deadline) {
                    self.performSegue(withIdentifier: "homeScreen", sender: self)
                }
                
               
            }
            
            
        }
        
        
    }
    
    
    @IBAction func logOut(_ sender: UIButton) {
        
        
        Digits.sharedInstance().logOut()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

