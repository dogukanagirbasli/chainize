//
//  UserContacts.swift
//  Chainize
//
//  Created by Doğukan Ağırbaşlı on 5/14/17.
//  Copyright © 2017 Doğukan Ağırbaşlı. All rights reserved.
//

import Foundation
import Contacts
import ContactsUI

class FetchUserContacts : NSObject, CNContactPickerDelegate{
    
        var contacts: [CNContact] = {
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey,
            CNContactImageDataAvailableKey,
            CNContactThumbnailImageDataKey] as [Any]
        
        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        
        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                
                
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching results for container")
            }
        }
        
        //print ( (results.first?.phoneNumbers)!)
        
        
       
        
        /*let size = results.count
        print(size)
        for index in 0..<size{
            let a = results[index].phoneNumbers.first?.value.stringValue
            print(a!)
        }*/
        
       
        return results
    }()
    
    
}
