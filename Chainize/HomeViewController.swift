//
//  HomeViewController.swift
//  Chainize
//
//  Created by Doğukan Ağırbaşlı on 5/26/17.
//  Copyright © 2017 Doğukan Ağırbaşlı. All rights reserved.
//

import UIKit


class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FetchData {
    
    
    @IBOutlet weak var myTable: UITableView!
    
    private var contacts = [Contact]()
    private let CHAT_SEGUE = "ChatSegue"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get users from firebase
        DBProvider.Instance.delegate=self
        DBProvider.Instance.getContacts()
        
        // Do any additional setup after loading the view.
        let x = FetchUserContacts()
        
        
        let size =  x.contacts.count
        //print(size)
        for index in 0..<size{
            if let a = x.contacts[index].phoneNumbers.first?.value.stringValue{
                print(a)
                let num=a.NumeralsOnly
                print("Num:")
                print(num)
            }
        }
            print(size)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dataRecieved(contacts: [Contact]) {
        
        self.contacts = contacts
        
        myTable.reloadData()
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        //cell.textLabel?.text = "OKKKK"
        cell.textLabel?.text = contacts[indexPath.row].name
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: CHAT_SEGUE, sender: nil)
        
    }
    

}
