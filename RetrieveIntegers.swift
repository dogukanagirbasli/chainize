//
//  RetrieveIntegers.swift
//  Chainize
//
//  Created by Doğukan Ağırbaşlı on 5/31/17.
//  Copyright © 2017 Doğukan Ağırbaşlı. All rights reserved.
//

import Foundation
import CoreFoundation


extension String {
    var NumeralsOnly: String {
        let pattern = UnicodeScalar("0")..."9"
        return String(unicodeScalars
            .flatMap { pattern ~= $0 ? Character($0) : nil })
    }
}

//getting digits from phone number
